import { RouterModule, Routes } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";
import { CurriculumComponent } from "./components/curriculum/curriculum.component";
import { ExperienciaComponent } from "./components/experiencia/experiencia.component";
import { NadvarComponent } from "./components/nadvar/nadvar.component";
import { EducacionComponent } from "./components/educacion/educacion.component";
import { FormularioComponent } from "./components/formulario/formulario.component";




const APP_ROUTES: Routes = [
    {path: 'curriculum', component: CurriculumComponent},
    {path: 'experiencia', component:ExperienciaComponent},
    {path: 'nadvar', component:NadvarComponent},
    {path: 'educacion', component:EducacionComponent},
    {path: 'formulario', component:FormularioComponent},
    {path: '**', pathMatch:'full', redirectTo:'curriculum'}
]




export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
